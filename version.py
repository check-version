class Version(object):
    def __init__(self, ver, debian=False):
        if debian and ver:
            # remove epoch and debian part
            if ':' in ver:
                ver = ver.split(':', 1)[1]
            if '-' in ver:
                ver = ver.split('-', 1)[0]
        self.ver = ver

    def __cmp__(self, other):
        if other is None or other.ver is None:
            return 1
        if self.ver is None:
            return -1
        if other.ver == self.ver:
            return 0

        nb1 = map(int, self.ver.split('.'))
        nb2 = map(int, other.ver.split('.'))

        for i, j in zip(nb1, nb2):
            if i < j:
                return -1
            elif i > j:
                return 1

        if len(nb1) < len(nb2):
            return -1
        elif len(nb1) > len(nb2):
            return 1

        return 0

    def __str__(self):
        return self.ver

if __name__ == '__main__':
    assert Version('1.0') < Version('2.0')
    assert Version('1.0') < Version('2.0')
    assert Version('1.0') != Version('2.0')
    assert Version('1.0') == Version('1.0')
    assert Version('1.1.0') < Version('2.0')
    assert Version('1.1.2') < Version('1.1.3')
    assert Version('1.3') < Version('1.3.1')
    assert max(Version('1.0'), Version('1.5'), Version('1.5.2')) == Version('1.5.2')
    assert max(Version('0.7.9'), Version('0.7.18')) == Version('0.7.18')
    assert Version(None) < Version('1.3.1')
    assert Version('1.4.1-3', debian=True) == Version('1.4.1')
