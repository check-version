import sys
import datetime

from report import Report
from version import Version
from component import Component

UBUNTU_STABLE = 'intrepid'
UBUNTU_DEV = 'jaunty'

class WebReport(object):
    def __init__(self, filename):
        self.report = Report()
        self.out = open(filename, 'w')

    def generate(self):
        self.out.write('<html>')
        self.out.write('''<style type="text/css">
tbody th {
  background: #d3d7cf;
  text-align: left;
}

tbody td {
  text-align: center;
}

td.dunno { background: #aaa; }
td.sync { background: #fcaf3e; }
td.uptodate { background: #8ae234; }
td.needsync { background: #3465a4; }
thead tr { background: #555753; color: white; }

</style>''')
        self.out.write('<body>')
        self.out.write('<h1>Telepathy components</h1>')
        self.out.write('<table>')

        # titles
        self.out.write('<thead>')
        self.out.write('<tr>')
        self.out.write('<th>Component</th>')
        self.out.write('<th>Upstream</th>')
        self.out.write('<th>Debian Sid</th>')
        self.out.write('<th>Debian Experimental</th>')
        self.out.write('<th>Ubuntu Jaunty</th>')
        self.out.write('<th>JHBuild</th>')
        if Component.ppa_enabled():
            self.out.write('<th>Ubuntu Stable (+PPA)</th>')
        self.out.write('</tr>')
        self.out.write('</thead>')

        self.out.write('<tbody>')
        for component in self.report.components:
            self.out.write('<tr>')
            self.out.write('<th>%s</th>' % component)
            self.out.write('<td class="uptodate">%s</td>' % component.upstream)

            # biggest Debian version
            debian_ver = max(component.debian.get('unstable'), component.debian.get('experimental'))

            for distro, distro_version in (
                    ('debian', 'unstable'),
                    ('debian', 'experimental'),
                    ('ubuntu', UBUNTU_DEV),
                    ('jhbuild', 'trunk')):
                version_number = getattr(component, distro).get(distro_version)
                if distro == 'ubuntu' and debian_ver == component.upstream and version_number < debian_ver:
                    # Need to sync Ubuntu package on Debian
                    status = 'needsync'
                elif distro == 'ubuntu' and debian_ver < component.upstream and version_number == debian_ver:
                    # Ubuntu package is sync with Debian but not with upstream
                    status = 'sync'
                elif version_number < component.upstream:
                    status = 'lagging'
                else:
                    status = 'uptodate'

                self.out.write('<td class="%s">%s</td>' % (status, version_number))

            if Component.ppa_enabled():
                # add Ubuntu stable + PPA
                ubuntu_stable_ver = max(component.ubuntu.get(UBUNTU_STABLE), component.ppa.get(UBUNTU_STABLE))
                if ubuntu_stable_ver == component.upstream:
                    status = 'uptodate'
                elif ubuntu_stable_ver == component.ubuntu.get(UBUNTU_DEV):
                    # sync with Ubuntu dev but not with upstream
                    status = 'sync'
                elif component.ubuntu.get(UBUNTU_DEV) == component.upstream:
                    # need to sync with Ubuntu dev
                    status = 'needsync'
                else:
                    status = 'lagging'

                self.out.write('<td class="%s">%s</td>' % (status, ubuntu_stable_ver))

            self.out.write('</tr>')
        self.out.write('</tbody>')
        self.out.write('</table>')

        # Legend
        self.out.write('<br/><br/>')
        self.out.write('<table>')
        self.out.write('<thead>')
        self.out.write('<tr><th>Legend</th></tr>')
        self.out.write('</thead>')
        self.out.write('<tr><td class="uptodate">Up to date</td><td>Package is up to date with upstream</td></tr>')
        self.out.write('<tr><td class="needsync">Need Sync</td><td>Package just have to be synced to be up to date</td></tr>')
        self.out.write('<tr><td class="sync">Sync</td><td>Package is synced but not up to date with upstream</td></tr>')
        self.out.write('</table>')

        self.out.write('<p><center>Generated: %s</center></p>' % datetime.datetime.utcnow().strftime('%d-%m-%y %H:%M:%S %z'))
        self.out.write('</body>')
        self.out.write('</html>')


if __name__ == '__main__':
    web = WebReport(sys.argv[1])
    web.generate()
