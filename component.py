import urllib2
import re
import commands
import os
import git

try:
    import elementtree.ElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET

from version import Version
try:
    from PPARequest import PPARequest
except ImportError:
    PPARequest = None
import config

LOCAL_GIT_REPOS_PATH = os.path.join(config.check_versions_dir, 'git')

class Component(object):
    """abstract class"""
    def __init__(self, name):
        self.name = name

        self.upstream = self._get_upstream_version()
        self.ubuntu = self._get_ubuntu_versions()
        self.debian = self._get_debian_versions()
        self.jhbuild = self._get_jhbuild_versions()

        if Component.ppa_enabled():
            self.ppa = self._get_ppa_versions()
        else:
            self.ppa = None

    def _get_upstream_version(self):
        raise NotImplemented

    def _get_jhbuild_versions(self):
        module_set = JhbuildModulesetCache.get_module_set()
        for child in module_set.getchildren():
            if child.attrib.get('id') != self.name:
                continue
            if child.attrib.get('version'):
                return {'trunk': Version(child.attrib.get('version'))}
            return {'trunk': Version(child.find('branch').attrib.get('version'))}
        return {}

    def _get_ubuntu_versions(self):
        return self._rmadison('ubuntu')

    def _get_debian_versions(self):
        return self._rmadison('debian')

    def _rmadison(self, distro):
        versions = {}
        (s, o) = commands.getstatusoutput('rmadison -u %s %s' % (distro, self.name))

        for l in o.split('\n'):
            try:
                pkg, ver, distro, archi = l.split('|')
            except ValueError:
                continue

            ver = ver.strip()
            distro = distro.strip()
            distro = distro.split('/')[0]

            versions[distro] = Version(ver, debian=True)

        return versions

    def _get_ppa_versions(self):
        return PPARequest().query(self.name)

    def __str__(self):
        return self.name

    @staticmethod
    def ppa_enabled():
        return 'CHECK_VERSION_LAUNCHPAD' in os.environ and PPARequest is not None

class HttpComponent(Component):
    def __init__(self, name, repo):
        self.repo = repo

        Component.__init__(self, name)

    def _get_upstream_version(self):
        # FIXME: there is probably a better regexp
        reg_exp = re.compile('%s-.*(?=.tar.gz">)' % self.name)
        listing = urllib2.urlopen(self.repo)

        versions = []
        for release in reg_exp.findall(listing.read()):
            version = re.split('%s-' % self.name, release)[1]
            versions.append(Version(version))

        return max(versions)

class GitComponent(Component):
    def __init__(self, name, remote_repo):
        self.remote_repo = remote_repo
        self.local_repo_path = os.path.join(LOCAL_GIT_REPOS_PATH, name)

        if os.path.exists(self.local_repo_path):
            # update repo
            os.chdir(self.local_repo_path)
            commands.getstatusoutput('git pull')
            self.local_repo = git.Repo(self.local_repo_path)
        else:
            self.local_repo = self._clone_repo()

        Component.__init__(self, name)

    def _clone_repo(self):
        if not os.path.exists(LOCAL_GIT_REPOS_PATH):
            print "create", LOCAL_GIT_REPOS_PATH
            os.makedirs(LOCAL_GIT_REPOS_PATH)

        print "clone", self.remote_repo
        cmd = 'git clone %s %s' % (self.remote_repo, self.local_repo_path)
        status, out = commands.getstatusoutput(cmd)

        return git.Repo(self.local_repo_path)

    def _get_upstream_version(self):
        versions = []

        for tag in self.local_repo.tags:
            try:
                version = tag.name.rsplit('-', 1)[1]
            except IndexError:
                # assume '_' is used to separate version (as Empathy does)
                # FIXME: this is horrible
                version = tag.name.split('_', 1)[1]
                version = version.replace('_', '.')

            versions.append(Version(version))

        return max(versions)

class TelepathyComponent(HttpComponent):
    def __init__(self, name):
        repo = 'http://telepathy.freedesktop.org/releases/%s' % name

        HttpComponent.__init__(self, name, repo)

class FarsightComponent(HttpComponent):
    def __init__(self, name):
        repo = 'http://farsight.freedesktop.org/releases/%s' % name

        HttpComponent.__init__(self, name, repo)

class NiceComponent(HttpComponent):
    def __init__(self, name):
        repo = 'http://nice.freedesktop.org/releases/'

        HttpComponent.__init__(self, name, repo)


class JhbuildModulesetCache:
    module_set = None
    def get_module_set(cls):
        if cls.module_set:
            return cls.module_set
        external_deps = urllib2.urlopen('http://svn.gnome.org/svn/jhbuild/trunk/modulesets/gnome-external-deps-2.26.modules').read()
        module_set = ET.fromstring(external_deps)
        cls.module_set = module_set
        return module_set
    get_module_set = classmethod(get_module_set)

if __name__ == '__main__':
    def display(comp):
        print comp, comp.upstream, comp.ubuntu, comp.debian, comp.ppa, comp.jhbuild

    display(TelepathyComponent('telepathy-gabble'))
    display(FarsightComponent('farsight2'))
    display(NiceComponent('libnice'))
    display(GitComponent('telepathy-sofiasip', 'git://git.collabora.co.uk/git/telepathy-sofiasip.git'))
    display(GitComponent('empathy', 'git://git.collabora.co.uk/git/empathy.git'))
