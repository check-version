import os.path
from launchpadlib import launchpad
from launchpadlib.credentials import Credentials

from version import Version
import config

class PPARequest(object):
    # singleton
    class __PPARequest:
        def __init__(self):
            credentials_path = os.path.join(config.check_versions_dir, 'credentials.txt')
            try:
                credentials = Credentials()
                credentials.load(open(credentials_path))
                self.lp = launchpad.Launchpad(credentials, launchpad.EDGE_SERVICE_ROOT)
            except IOError:
                self.lp = launchpad.Launchpad.get_token_and_login("check-versions",
                        launchpad.EDGE_SERVICE_ROOT)

                self.lp.credentials.save(open(credentials_path, 'w'))

            tp_team = self.lp.people['telepathy']
            self.archive = tp_team.archive

        def query(self, name):
            result = {}
            pubs = self.archive.getPublishedSources(source_name=name, status=u"Published")

            for pub in pubs:
                distroseries = pub.distroseries
                if not distroseries.active:
                    continue
                series_name = distroseries.name
                version = pub.source_package_version
                result[series_name] = Version(version, debian=True)

            return result

    instance = None
    def __new__(c):
        if not PPARequest.instance:
            PPARequest.instance = PPARequest.__PPARequest()
        return PPARequest.instance

    def __getattr__(self, attr):
        return getattr(self.instance, attr)

    def __setattr__(self, attr, val):
        return setattr(self.instance, attr, val)


if __name__ == '__main__':
    print PPARequest().query('telepathy-gabble')
