from component import TelepathyComponent, FarsightComponent, NiceComponent, GitComponent

telepathy_projects = ['telepathy-gabble', 'telepathy-salut', 'telepathy-glib', 'telepathy-python',
    'telepathy-haze', 'telepathy-butterfly', 'telepathy-idle', 'telepathy-spec', 'telepathy-farsight',
    'pymsn']

farsight_projects = ['farsight2', 'gst-plugins-farsight']

class Report(object):
    def __init__(self):
        self.components = []

        for tp in telepathy_projects:
            self.components.append(TelepathyComponent(tp))

        for fs in farsight_projects:
            self.components.append(FarsightComponent(fs))

        self.components.append(NiceComponent('libnice'))
        self.components.append(GitComponent('telepathy-sofiasip', 'git://git.collabora.co.uk/git/telepathy-sofiasip.git'))
        self.components.append(GitComponent('empathy', 'git://git.collabora.co.uk/git/empathy.git'))

if __name__ == '__main__':
    report = Report()
    for comp in report.components:
        print comp, comp.upstream, comp.ubuntu, comp.debian
